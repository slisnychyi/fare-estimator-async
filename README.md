
### note
_test implementation. without unit/e2e tests,documentation, valid backpressure_


### technology stack

- java
    - concurrency support (executor_service, parallel stream)
- [project reactor](http://projectreactor.io/) - reactive library, based on the [reactive_streams](https://www.reactive-streams.org/)
  specification, for building asynchronious non-blocking applications
- docker


### design overview
![img.png](img.png)


### how to run
- run from docker local image

0. clone git repository
1. run `./mvnw clean install`
2. run `docker build -t fare-estimator-async .` to create an image
3. run `docker run -v ${directory_path_with_source_file}:/app fare-estimator-async /app/${file_name}`

example:
```shell
./mvnw clean install && docker build -t fare-estimator-async .
docker run -v /usr/projects/fare-estimator-async/resources:/app fare-estimator-async /app/paths.csv

```

- run from docker remote image

link : https://hub.docker.com/r/slisnychyi/fare-estimator-async

example:
```shell
docker pull slisnychyi/fare-estimator-async
docker run -v /usr/projects/fare-estimator-async/resources:/app slisnychyi/fare-estimator-async /app/paths.csv

```

