package com.slisnychyi;

import com.slisnychyi.model.RideSegment;
import com.slisnychyi.model.Tuple;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Runner {
    private static final ExecutorService RIDE_SEGMENTS_THREAD_POOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public static void main(String[] args) throws IOException {
        RideFareCalculator fareCalculator = new RideFareCalculator();
        Instant start = Instant.now();

        if (args.length < 1) throw new IllegalArgumentException("source file argument was missed.");
        Path sourcePath = Paths.get(args[0]);
        if (sourcePath.toFile().exists()) {
            System.out.printf("Start processing rides [from=%s, size=%s bytes]\n", sourcePath, Files.size(sourcePath));

            try (BufferedReader reader = new BufferedReader(new FileReader(sourcePath.toFile()))) {
                Map<Integer, List<Tuple>> collect = reader.lines()
                        .parallel()
                        .map(Tuple::generate)
                        .collect(Collectors.groupingBy(Tuple::id));

                Path resultPath = Paths.get(sourcePath.getParent() + "/result.csv");
                BufferedWriter bw = Files.newBufferedWriter(resultPath);
                Flux.fromStream(() -> collect.entrySet().stream())
                        .flatMap(tps -> Flux.fromIterable(tps.getValue())
                                .subscribeOn(Schedulers.fromExecutor(RIDE_SEGMENTS_THREAD_POOL))
                                .buffer(2, 1)
                                .filter(Runner::validTuples)
                                .map(tuples -> {
                                    Tuple prev = tuples.get(0);
                                    Tuple cur = tuples.get(1);
                                    return RideSegment.generate(prev, cur);
                                })
                                .filter(e -> e.speed().doubleValue() <= 100)
                                .map(fareCalculator::calculateFare)
                                .reduce(BigDecimal::add)
                                .map(e -> e.doubleValue() < 3.47 ? BigDecimal.valueOf(3.47) : e.setScale(2, RoundingMode.HALF_EVEN))
                                .map(e -> String.format("%s, %s\n", tps.getKey(), e))
                        )
                        .subscribe(e -> write(bw, e), e -> close(bw), () -> {
                            System.out.printf("Result was persisted [to=%s, in=%s]\n", resultPath, Duration.between(start, Instant.now()));
                            close(bw);
                        });
            }
        } else {
            throw new IllegalArgumentException("No source file exists = " + sourcePath);
        }
    }

    private static void close(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static void write(BufferedWriter bw, String string) {
        try {
            bw.write(string);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static boolean validTuples(List<Tuple> tuples) {
        return tuples.size() > 1 && tuples.get(0).timestamp() != tuples.get(1).timestamp();
    }

}

