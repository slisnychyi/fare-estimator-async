package com.slisnychyi;

import com.slisnychyi.model.RideSegment;
import com.slisnychyi.util.RideSegmentUtils;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.time.ZoneOffset.UTC;

public class RideFareCalculator {
    private static final int IDLE_SPEED = 10;
    private static final String FIVE_AM = "05:00";
    private static final BigDecimal IDLE_FARE = new BigDecimal("11.9");
    private static final BigDecimal DAY_FARE = new BigDecimal("0.74");
    private static final BigDecimal NIGHT_FARE = new BigDecimal("1.3");

    /**
     * Calculates fare for the ride segment with the following rules.
     * 1. Time of day (05:00, 00:00] - DAY_FARE
     * 2. Time of day (00:00, 05:00] - NIGHT_FARE
     * 3. Speed <= 10km/h - IDLE_FARE
     * 4. Start (05:00, 00:00] and Finish (00:00, 05:00] - DAY_FARE + NIGHT_FARE
     * 5. Start (00:00, 05:00] and Finish (05:00, 00:00] - NIGHT_FARE + DAY_FARE
     * @param segment of the ride
     * @return calculated fare for the segment
     */
    public BigDecimal calculateFare(RideSegment segment) {
        LocalDateTime start = LocalDateTime.ofInstant(Instant.ofEpochSecond(segment.startTimestamp()), UTC);
        LocalDateTime finish = LocalDateTime.ofInstant(Instant.ofEpochSecond(segment.finishTimestamp()), UTC);
        if (segment.speed().doubleValue() <= IDLE_SPEED) {
            return IDLE_FARE.multiply(segment.time());
        }
        if (start.toLocalDate().isEqual(finish.toLocalDate())) {
            if(start.getHour() >= 5) return segment.distance().multiply(DAY_FARE);
            if(finish.getHour() < 5) return segment.distance().multiply(NIGHT_FARE);
            LocalDateTime fiveAm = start.toLocalDate().atTime(LocalTime.parse(FIVE_AM));
            return calculateByTimeParts(start, finish, fiveAm, segment.speed());
        }
        LocalDateTime startOfDay = finish.toLocalDate().atStartOfDay();
        return calculateByTimeParts(start, finish, startOfDay, segment.speed());
    }

    private BigDecimal calculateByTimeParts(LocalDateTime start, LocalDateTime finish, LocalDateTime middle, BigDecimal speed) {
        BigDecimal dayFare = RideSegmentUtils.secondsToHours(Duration.between(start, middle).toSeconds())
                .multiply(speed).multiply(DAY_FARE);
        BigDecimal nightFare = RideSegmentUtils.secondsToHours(Duration.between(middle, finish).toSeconds())
                .multiply(speed).multiply(NIGHT_FARE);
        return dayFare.add(nightFare);
    }
}
