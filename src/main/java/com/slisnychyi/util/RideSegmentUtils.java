package com.slisnychyi.util;

import java.math.BigDecimal;
import java.util.Optional;

public class RideSegmentUtils {
    private static final BigDecimal HOURS_TRANSFORMER = new BigDecimal("0.000277778");

    public static BigDecimal getHaversineDistance(double lat1, double lon1, double lat2, double lon2) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return BigDecimal.ZERO;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1))
                    * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return BigDecimal.valueOf(dist);
        }
    }

    public static BigDecimal secondsToHours(long seconds) {
        return Optional.of(seconds)
                .filter(e -> e > 0L)
                .map(BigDecimal::valueOf)
                .map(HOURS_TRANSFORMER::multiply)
                .orElseThrow(() ->
                        new IllegalArgumentException(String.format("unable to transform seconds[%s] to hours", seconds)));
    }
}
