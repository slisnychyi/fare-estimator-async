package com.slisnychyi.model;

import com.slisnychyi.util.RideSegmentUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public record RideSegment(long startTimestamp,
                          long finishTimestamp,
                          BigDecimal time,
                          BigDecimal distance,
                          BigDecimal speed) {
    public static RideSegment generate(Tuple from, Tuple to) {
        BigDecimal time = RideSegmentUtils.secondsToHours(to.timestamp() - from.timestamp());
        BigDecimal distance = RideSegmentUtils.getHaversineDistance(from.lat(), from.lon(), to.lat(), to.lon());
        BigDecimal speed = distance.divide(time, RoundingMode.HALF_EVEN);
        return new RideSegment(from.timestamp(), to.timestamp(), time, distance, speed);
    }
}
