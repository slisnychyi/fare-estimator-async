package com.slisnychyi.model;

import java.util.Optional;

public record Tuple(int id, double lat, double lon, long timestamp) {
    public static Tuple generate(String value) {
        return Optional.ofNullable(value)
                .map(e -> e.split(","))
                .filter(e -> e.length == 4)
                .map(e -> new Tuple(Integer.parseInt(e[0]), Double.parseDouble(e[1]),
                        Double.parseDouble(e[2]), Long.parseLong(e[3])))
                .orElseThrow(() -> new IllegalArgumentException("unable to create tuple from = " + value));

    }
}