FROM openjdk:17-oracle
COPY target/*.jar fare-estimator-async.jar
ENTRYPOINT ["java","-jar","/fare-estimator-async.jar"]